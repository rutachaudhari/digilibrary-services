package com.samarthanam.digitallibrary.dto.request;

import lombok.Getter;

@Getter
public class UserForgotPasswordRequestDto {

    private String email;
}
