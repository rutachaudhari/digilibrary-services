package com.samarthanam.digitallibrary.constant;

public class RequestConstants {

    /* Paths */
    public static final String SIGNUP_PATH = "/signup";
    public static final String SIGNUP_VERIFY_PATH = "/signup/verify";
    public static final String LOGIN_PATH = "/login";
    public static final String FORGOT_PASSWORD_PATH = "/password/forgot";
    public static final String RESET_PASSWORD_LINK = "/password/reset";
    public static final String UPDATE_PASSWORD_PATH = "/password/update";
    /* Request Headers */
    public static final String APPLICATION_JSON = "application/json";
}
