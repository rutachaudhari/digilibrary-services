package com.samarthanam.digitallibrary.constant;

public class ServiceConstants {

    public static final String VERIFICATION_STATUS_SUCCESS = "success";

    /* Email template constants */
    public static final String SIGNUP_VERIFY_LINK = "verifySignupLink";
    public static final String FORGOT_PASSWORD_LINK = "forgotPasswordLink";
}
